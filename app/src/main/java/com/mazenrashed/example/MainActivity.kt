package com.mazenrashed.example

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.google.zxing.BarcodeFormat
import com.google.zxing.qrcode.QRCodeWriter
import com.mazenrashed.printooth.Printooth
import com.mazenrashed.printooth.data.converter.ArabicConverter
import com.mazenrashed.printooth.data.printable.ImagePrintable
import com.mazenrashed.printooth.data.printable.Printable
import com.mazenrashed.printooth.data.printable.RawPrintable
import com.mazenrashed.printooth.data.printable.TextPrintable
import com.mazenrashed.printooth.data.printer.DefaultPrinter
import com.mazenrashed.printooth.ui.ScanningActivity
import com.mazenrashed.printooth.utilities.Printing
import com.mazenrashed.printooth.utilities.PrintingCallback
import com.mazenrashed.universalbluetoothprinter.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.util.*
import kotlin.collections.ArrayList


class MainActivity : AppCompatActivity() {

    private var printing : Printing? = null
    val c = Calendar.getInstance()
    private val year = c.get(Calendar.YEAR)
    private val month = c.get(Calendar.MONTH)
    private val day = c.get(Calendar.DAY_OF_MONTH)
    var partNoSelect: String? = null
    @RequiresApi(Build.VERSION_CODES.O)
    val current : LocalDateTime = LocalDateTime.now()
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (Printooth.hasPairedPrinter())
            printing = Printooth.printer()
        val bind:BindSpin = BindSpin(this)
        TvpartNo.isEnabled = false
        dateTv.isEnabled = false
        dateTv2.isEnabled = false
        dateTv3.isEnabled = false
        val curDateNow = current.dayOfMonth.toString()+"/"+current.monthValue.toString()+"/"+current.year.toString()
        dateTv.setText(curDateNow)
        dateTv2.setText("-")
        dateTv3.setText("-")
        initViews()
        initListeners()
        setActionbar()
        bindDataSpinner()
        bindspinQty1()
        bindspinQty2()
        bindspinQty3()
        setupPickDate()
    }
    fun setupPickDate(){
        pickDateBtn.setOnClickListener{
            val dpd = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                // Display Selected date in TextView
                val selectedDate = "$dayOfMonth/${monthOfYear+1}/$year"
                dateTv.setText("" + dayOfMonth + "/" + (monthOfYear + 1) + "/" + year)
                //val sdf = SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH)
                //val theDate = sdf.parse(selectedDate)
            }, year, month, day)
            dpd.show()
        }
        pickDateBtn2.setOnClickListener{
            val dpd = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                val selectedDate = "$dayOfMonth/${monthOfYear+1}/$year"
                dateTv2.setText("" + dayOfMonth + "/" + (monthOfYear + 1) + "/" + year)
            }, year, month, day)
            dpd.show()
        }
        pickDateBtn3.setOnClickListener{
            val dpd = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                val selectedDate = "$dayOfMonth/${monthOfYear+1}/$year"
                dateTv3.setText("" + dayOfMonth + "/" + (monthOfYear + 1) + "/" + year)
                //val sdf = SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH)
                //val theDate = sdf.parse(selectedDate)
            }, year, month, day)
            dpd.show()
        }
    }
    fun bitmapToFile(bitmap: Bitmap, fileNameToSave: String): File? { // File name like "image.png"
        //create a file to write bitmap data
        var file: File? = null

        return try {
            file = File(Environment.getExternalStorageDirectory().toString() + File.separator + fileNameToSave)
            //file = File(Environment.getDataDirectory().toString() + File.separator + fileNameToSave)
            print(file.toString())
            file.createNewFile()
            Toast.makeText(this, file.toString(), Toast.LENGTH_LONG).show()
            //imageView1.setImageDrawable(bitmapToDrawable(bitmap))

            //Convert bitmap to byte array
            val bos = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.PNG, 0, bos) // YOU can also save it in JPEG
            val bitmapdata = bos.toByteArray()

            //write the bytes in file
            val fos = FileOutputStream(file)
            fos.write(bitmapdata)
            fos.flush()
            fos.close()
            file
        } catch (e: Exception) {
            e.printStackTrace()
            file // it will return null
        }
    }

    private fun setActionbar(){
        val actionBar = supportActionBar
        actionBar!!.title = ""
        actionBar!!.setDisplayShowHomeEnabled(true)
        actionBar!!.setLogo(R.drawable.sati)
        actionBar!!.setDisplayUseLogoEnabled(true)
        val colorDrawable = ColorDrawable(Color.parseColor("#ffffff"))
        actionBar!!.setBackgroundDrawable(colorDrawable)
    }
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here.
        val id = item.getItemId()
        if (id == R.id.mnadd) {

            val intent = Intent(this, AddActivity::class.java)
            startActivity(intent)
           // val current : String = LocalDateTime.now().toString()
            /*
            Toast.makeText(this, current, Toast.LENGTH_LONG).show()
            return true

             */


        }
        return super.onOptionsItemSelected(item)

    }

    /**
     * Function bind data Spinner
     */
    private fun bindDataSpinner(){
        val databaseHandlerini: DatabaseHandler = DatabaseHandler(this)
        val arraySpinner : ArrayList<String> = ArrayList()
        var partSpinner:ArrayList<PartModelClass> = databaseHandlerini.viewPart()
        arraySpinner.add("Partname           ")
        for(i in partSpinner){
            arraySpinner.add(i.partName)
        }
        val adapterspin = ArrayAdapter(this,android.R.layout.simple_spinner_item,arraySpinner)
        adapterspin.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line)
        spinner.adapter = adapterspin
        spinner.onItemSelectedListener = object: AdapterView.OnItemSelectedListener{
            @RequiresApi(Build.VERSION_CODES.O)
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long){
                // Display the selected item text on text view
               // text_spin.text = "selected : ${parent.getItemAtPosition(position).toString()}"
                //TvpartName.setText(parent.getItemAtPosition(position).toString())
                //TvpartNo!!.text = "Select"+arraySpinner[position]
                val curDateNowchange = current.dayOfMonth.toString()+"/"+current.monthValue.toString()+"/"+current.year.toString()
                var partSelect: PartModelClass? = databaseHandlerini.viewPartByName(arraySpinner[position].toString())
                partNoSelect = partSelect?.partName.toString()
                TvpartNo.setText(partSelect?.partNo.toString())
                dateTv.setText(curDateNowchange)
                dateTv2.setText("-")
                dateTv3.setText("-")

            }

            override fun onNothingSelected(parent: AdapterView<*>){
                // Another interface callback
            }
        }

    }
    private fun bindspinQty1(){
        val arraySpinner : ArrayList<String> = ArrayList()
        for(x in 0..300){
            arraySpinner.add(x.toString())
        }
        val adapterspin = ArrayAdapter(this,android.R.layout.simple_spinner_item,arraySpinner)
        adapterspin.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line)
        spinnerQty1.adapter = adapterspin
        spinnerQty1.onItemSelectedListener = object: AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(p0: AdapterView<*>?) {
                //TODO("Not yet implemented")
            }

            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                //etQty1.setText(arraySpinner[position].toString())

            }

        }


    }
    private fun bindspinQty2(){
        val arraySpinner : ArrayList<String> = ArrayList()
        for(x in 0..300){
            arraySpinner.add(x.toString())
        }
        val adapterspin = ArrayAdapter(this,android.R.layout.simple_spinner_item,arraySpinner)
        adapterspin.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line)
        spinnerQty2.adapter = adapterspin
        spinnerQty2.onItemSelectedListener = object: AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(p0: AdapterView<*>?) {
                //TODO("Not yet implemented")
            }

            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                //etQty2.setText(arraySpinner[position].toString())

            }

        }


    }
    private fun bindspinQty3(){
        val arraySpinner : ArrayList<String> = ArrayList()
        for(x in 0..300){
            arraySpinner.add(x.toString())
        }
        val adapterspin = ArrayAdapter(this,android.R.layout.simple_spinner_item,arraySpinner)
        adapterspin.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line)
        spinnerQty3.adapter = adapterspin
        spinnerQty3.onItemSelectedListener = object: AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(p0: AdapterView<*>?) {
                //TODO("Not yet implemented")
            }

            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                //etQty3.setText(arraySpinner[position].toString())

            }

        }


    }



    private fun initViews() {
       // btnPiarUnpair.text = if (Printooth.hasPairedPrinter()) "Un-pair ${Printooth.getPairedPrinter()?.name}" else "Pair with printer"

        if (Printooth.hasPairedPrinter()) printBtn.setImageResource(R.drawable.ic_action_printon) else printBtn.setImageResource(R.drawable.ic_action_printoff)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun initListeners() {
        /*
        btnPrint.setOnClickListener {
            if (!Printooth.hasPairedPrinter()) startActivityForResult(Intent(this,
                    ScanningActivity::class.java),
                    ScanningActivity.SCANNING_FOR_PRINTER)
            else printSomePrintable()
        }

        btnPrintImages.setOnClickListener {
            if (!Printooth.hasPairedPrinter()) startActivityForResult(Intent(this,
                    ScanningActivity::class.java),
                    ScanningActivity.SCANNING_FOR_PRINTER)
            else printSomeImages()
        }

        btnPiarUnpair.setOnClickListener {
            if (Printooth.hasPairedPrinter()) Printooth.removeCurrentPrinter()
            else startActivityForResult(Intent(this, ScanningActivity::class.java),
                    ScanningActivity.SCANNING_FOR_PRINTER)
            initViews()
        }
        btnCustomPrinter.setOnClickListener {
            startActivity(Intent(this, WoosimActivity::class.java))
        }

         */
        BtnactionPint.setOnClickListener {
            val databaseHandlerini: DatabaseHandler = DatabaseHandler(this)
            if (!Printooth.hasPairedPrinter()) {
                startActivityForResult(Intent(this,
                        ScanningActivity::class.java),
                        ScanningActivity.SCANNING_FOR_PRINTER)
            }

            else{
                var partSelect: PartModelClass? = databaseHandlerini.viewPartByName(partNoSelect.toString())
                val print = PrintModelClass(date1 = dateTv.text.toString(),date2 = dateTv2.text.toString(),
                        date3 = dateTv3.text.toString(),partno = partSelect?.partNo.toString(),
                        partname = partSelect?.partName.toString(),fgcode = partSelect?.fgpdCode.toString(),
                        linename = partSelect?.lineName.toString()
                )
                printSomeImages(print)
            }
        }
        printBtn.setOnClickListener {
            if (Printooth.hasPairedPrinter()) Printooth.removeCurrentPrinter()
            else startActivityForResult(Intent(this, ScanningActivity::class.java),
                    ScanningActivity.SCANNING_FOR_PRINTER)
            initViews()
        }



        printing?.printingCallback = object : PrintingCallback {
            override fun connectingWithPrinter() {
                Toast.makeText(this@MainActivity, "Connecting with printer", Toast.LENGTH_SHORT).show()
            }

            override fun printingOrderSentSuccessfully() {
                Toast.makeText(this@MainActivity, "Order sent to printer", Toast.LENGTH_SHORT).show()
            }

            override fun connectionFailed(error: String) {
                Toast.makeText(this@MainActivity, "Failed to connect printer", Toast.LENGTH_SHORT).show()
            }

            override fun onError(error: String) {
                Toast.makeText(this@MainActivity, error, Toast.LENGTH_SHORT).show()
            }

            override fun onMessage(message: String) {
                Toast.makeText(this@MainActivity, "Message: $message", Toast.LENGTH_SHORT).show()
            }

        }
    }


    private fun printSomePrintable() {
        val printables = getSomePrintables()
        printing?.print(printables)
    }
    private fun bitmapToDrawable(bitmap:Bitmap):BitmapDrawable{
        return BitmapDrawable(resources,bitmap)
    }
    @RequiresApi(Build.VERSION_CODES.O)
    private fun printSomeImages(prn:PrintModelClass) {
        val currentPrint : LocalDateTime = LocalDateTime.now()
        val curDatePrintNow = currentPrint.dayOfMonth.toString()+"/"+currentPrint.monthValue.toString()+
                "/"+currentPrint.year.toString()+" "+currentPrint.hour.toString()+":"+currentPrint.minute.toString()

        if(prn.partno == ""){
            Toast.makeText(this@MainActivity, "Select Part Name", Toast.LENGTH_SHORT).show()
            return
        }
        val printables = ArrayList<Printable>().apply {
            //val content = "21/10/2020:DHA9-33-031:KNUCKLELH:ZB2107072:KNUCKLI 13"
            val content = prn.date1+"::"+spinnerQty1.selectedItem.toString() +"::"+prn.date2+"::"+spinnerQty2.selectedItem.toString()+
                    "::"+prn.date3+"::"+spinnerQty3.selectedItem.toString()+"::"+prn.partno+"::"+prn.partname+
                    "::"+prn.fgcode+"::"+prn.linename+"::"+ curDatePrintNow
            val writer = QRCodeWriter()
            val bitMatrix = writer.encode(content, BarcodeFormat.QR_CODE, 255, 255)
            val width = bitMatrix.width
            val height = bitMatrix.height
            val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565)
            for (x in 0 until width) {
                for (y in 0 until height) {
                    bitmap.setPixel(x, y, if (bitMatrix.get(x, y)) Color.BLACK else Color.WHITE)
                }
            }
            add(TextPrintable.Builder()
                    .setText("\n\n\n\n\n")
                    .setCharacterCode(DefaultPrinter.CHARCODE_PC1252)
                    .setNewLinesAfter(1)
                    .build())

            add(TextPrintable.Builder()
                    .setText("Line Name :  ")
                    .setCharacterCode(DefaultPrinter.CHARCODE_PC1252)
                    .setAlignment(DefaultPrinter.ALIGNMENT_CENTER)
                    .setEmphasizedMode(DefaultPrinter.EMPHASIZED_MODE_BOLD)
                    .build())
            add(TextPrintable.Builder()
                    .setText(prn.linename)
                    .setCharacterCode(DefaultPrinter.CHARCODE_PC1252)
                    .setUnderlined(DefaultPrinter.UNDERLINED_MODE_ON)
                    .setNewLinesAfter(1)
                    .build())
            ///////////////////////////////////////////////////////////////

            add(TextPrintable.Builder()
                    .setText("Part Name :  ")
                    .setCharacterCode(DefaultPrinter.CHARCODE_PC1252)
                    .setAlignment(DefaultPrinter.ALIGNMENT_CENTER)
                    .setEmphasizedMode(DefaultPrinter.EMPHASIZED_MODE_BOLD)
                    .build())
            add(TextPrintable.Builder()
                    .setText(prn.partname)
                    .setCharacterCode(DefaultPrinter.CHARCODE_PC1252)
                    .setUnderlined(DefaultPrinter.UNDERLINED_MODE_ON)
                    .setNewLinesAfter(1)
                    .build())
            ///////////////////////////////////////////////////////////////
            add(TextPrintable.Builder()
                    .setText("Part No :  ")
                    .setCharacterCode(DefaultPrinter.CHARCODE_PC1252)
                    .setAlignment(DefaultPrinter.ALIGNMENT_CENTER)
                    .setEmphasizedMode(DefaultPrinter.EMPHASIZED_MODE_BOLD)
                    .build())
            add(TextPrintable.Builder()
                    .setText(prn.partno)
                    .setCharacterCode(DefaultPrinter.CHARCODE_PC1252)
                    .setUnderlined(DefaultPrinter.UNDERLINED_MODE_ON)
                    .setNewLinesAfter(1)
                    .build())

            ////////////////////////////////////////////////////////
            add(TextPrintable.Builder()
                    .setText("M/C Date1 :  ")
                    .setCharacterCode(DefaultPrinter.CHARCODE_PC1252)
                    .setAlignment(DefaultPrinter.ALIGNMENT_CENTER)
                    .setEmphasizedMode(DefaultPrinter.EMPHASIZED_MODE_BOLD)
                    .build())
            add(TextPrintable.Builder()
                    .setText(prn.date1)
                    .setCharacterCode(DefaultPrinter.CHARCODE_PC1252)
                    .setUnderlined(DefaultPrinter.UNDERLINED_MODE_ON)
                    .build())
            add(TextPrintable.Builder()
                    .setText(" Qty: ")
                    .setCharacterCode(DefaultPrinter.CHARCODE_PC1252)
                    .setAlignment(DefaultPrinter.ALIGNMENT_CENTER)
                    .setEmphasizedMode(DefaultPrinter.EMPHASIZED_MODE_BOLD)
                    .build())
            add(TextPrintable.Builder()
                    .setText(spinnerQty1.selectedItem.toString())
                    .setCharacterCode(DefaultPrinter.CHARCODE_PC1252)
                    .setUnderlined(DefaultPrinter.UNDERLINED_MODE_ON)
                    .setNewLinesAfter(1)
                    .build())
            ////////////////////////////////////////////////////////
            add(TextPrintable.Builder()
                    .setText("M/C Date2 :  ")
                    .setCharacterCode(DefaultPrinter.CHARCODE_PC1252)
                    .setAlignment(DefaultPrinter.ALIGNMENT_CENTER)
                    .setEmphasizedMode(DefaultPrinter.EMPHASIZED_MODE_BOLD)
                    .build())
            add(TextPrintable.Builder()
                    .setText(prn.date2)
                    .setCharacterCode(DefaultPrinter.CHARCODE_PC1252)
                    .setUnderlined(DefaultPrinter.UNDERLINED_MODE_ON)
                    .build())
            add(TextPrintable.Builder()
                    .setText(" Qty: ")
                    .setCharacterCode(DefaultPrinter.CHARCODE_PC1252)
                    .setAlignment(DefaultPrinter.ALIGNMENT_CENTER)
                    .setEmphasizedMode(DefaultPrinter.EMPHASIZED_MODE_BOLD)
                    .build())
            add(TextPrintable.Builder()
                    .setText(spinnerQty2.selectedItem.toString())
                    .setCharacterCode(DefaultPrinter.CHARCODE_PC1252)
                    .setUnderlined(DefaultPrinter.UNDERLINED_MODE_ON)
                    .setNewLinesAfter(1)
                    .build())
            ////////////////////////////////////////////////////////
            add(TextPrintable.Builder()
                    .setText("M/C Date3 :  ")
                    .setCharacterCode(DefaultPrinter.CHARCODE_PC1252)
                    .setAlignment(DefaultPrinter.ALIGNMENT_CENTER)
                    .setEmphasizedMode(DefaultPrinter.EMPHASIZED_MODE_BOLD)
                    .build())
            add(TextPrintable.Builder()
                    .setText(prn.date3)
                    .setCharacterCode(DefaultPrinter.CHARCODE_PC1252)
                    .setUnderlined(DefaultPrinter.UNDERLINED_MODE_ON)
                    .build())
            add(TextPrintable.Builder()
                    .setText(" Qty: ")
                    .setCharacterCode(DefaultPrinter.CHARCODE_PC1252)
                    .setAlignment(DefaultPrinter.ALIGNMENT_CENTER)
                    .setEmphasizedMode(DefaultPrinter.EMPHASIZED_MODE_BOLD)
                    .build())
            add(TextPrintable.Builder()
                    .setText(spinnerQty3.selectedItem.toString())
                    .setCharacterCode(DefaultPrinter.CHARCODE_PC1252)
                    .setUnderlined(DefaultPrinter.UNDERLINED_MODE_ON)
                    .setNewLinesAfter(1)
                    .build())
            ////////////////////////////////////////////////////////////
            add(TextPrintable.Builder()
                    .setText(curDatePrintNow)
                    .setCharacterCode(DefaultPrinter.CHARCODE_PC1252)
                    .setAlignment(DefaultPrinter.ALIGNMENT_CENTER)
                    .setUnderlined(DefaultPrinter.UNDERLINED_MODE_ON)
                    .setNewLinesAfter(1)
                    .build())

            add(ImagePrintable.Builder(bitmap)
                    .setAlignment(DefaultPrinter.ALIGNMENT_CENTER)
                    .build())

            add(TextPrintable.Builder()
                    .setText("\n\n\n\n")
                    .setCharacterCode(DefaultPrinter.CHARCODE_PC1252)
                    .setNewLinesAfter(1)
                    .build())
            //add(ImagePrintable.Builder(R.drawable.image2, resources).build())
           //add(ImagePrintable.Builder(R.drawable.image3, resources).build())
            getSomePrintables()
        }
        Toast.makeText(this@MainActivity, TvpartNo.text.toString(), Toast.LENGTH_SHORT).show()
        printing?.print(printables)
    }

    private fun getSomePrintables() = ArrayList<Printable>().apply {
        add(RawPrintable.Builder(byteArrayOf(27, 100, 4)).build()) // feed lines example in raw mode

        add(TextPrintable.Builder()
                .setText("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa")
                .setCharacterCode(DefaultPrinter.CHARCODE_PC1252)
                .setNewLinesAfter(1)
                .build())

        add(TextPrintable.Builder()
                .setText("Hello World : ATFB")
                .setCharacterCode(DefaultPrinter.CHARCODE_PC1252)
                .setNewLinesAfter(1)
                .build())

        add(TextPrintable.Builder()
                .setText("Hello World")
                .setLineSpacing(DefaultPrinter.LINE_SPACING_60)
                .setAlignment(DefaultPrinter.ALIGNMENT_CENTER)
                .setEmphasizedMode(DefaultPrinter.EMPHASIZED_MODE_BOLD)
                .setUnderlined(DefaultPrinter.UNDERLINED_MODE_ON)
                .setNewLinesAfter(1)
                .build())

        add(TextPrintable.Builder()
                .setText("Hello World")
                .setAlignment(DefaultPrinter.ALIGNMENT_RIGHT)
                .setEmphasizedMode(DefaultPrinter.EMPHASIZED_MODE_BOLD)
                .setUnderlined(DefaultPrinter.UNDERLINED_MODE_ON)
                .setNewLinesAfter(1)
                .build())

        add(TextPrintable.Builder()
                .setText("اa1224")
                .setAlignment(DefaultPrinter.ALIGNMENT_CENTER)
                .setEmphasizedMode(DefaultPrinter.EMPHASIZED_MODE_BOLD)
                .setFontSize(DefaultPrinter.FONT_SIZE_NORMAL)
                .setUnderlined(DefaultPrinter.UNDERLINED_MODE_ON)
                .setCharacterCode(DefaultPrinter.CHARCODE_ARABIC_FARISI)
                .setNewLinesAfter(1)
                .setCustomConverter(ArabicConverter()) // change only the converter for this one
                .build())
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == ScanningActivity.SCANNING_FOR_PRINTER && resultCode == Activity.RESULT_OK)
           // printSomePrintable()
        initViews()
    }
}
