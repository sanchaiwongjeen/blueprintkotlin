package com.mazenrashed.universalbluetoothprinter

import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.LinearLayoutManager
import com.mazenrashed.example.MainActivity
import com.mazenrashed.example.R
import kotlinx.android.synthetic.main.contect_add.*
import kotlinx.android.synthetic.main.dialog_update.*
import java.time.LocalDateTime

class AddActivity : AppCompatActivity() {
    @RequiresApi(Build.VERSION_CODES.O)
    val current : LocalDateTime = LocalDateTime.now()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add)
        btnAdd.setOnClickListener {

            addRecord()
        }
        //val Bind:BindSpin = BindSpin(this)
        setupListofDataIntoRecyclerView()
        setActionbar()
    }
    private fun setActionbar(){
        val actionBar = supportActionBar
        actionBar!!.title = ""
        actionBar!!.setDisplayShowHomeEnabled(true)
        actionBar!!.setLogo(R.drawable.sati)
        actionBar!!.setDisplayUseLogoEnabled(true)
        val colorDrawable = ColorDrawable(Color.parseColor("#ffffff"))
        actionBar!!.setBackgroundDrawable(colorDrawable)
    }
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here.
        val id = item.getItemId()
        if (id == R.id.mnhome) {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }

        if (id == R.id.mnadd) {

            val intent = Intent(this, AddActivity::class.java)
            startActivity(intent)


            Toast.makeText(this, current.dayOfMonth.toString()+"/"+current.monthValue.toString(), Toast.LENGTH_LONG).show()
            return true

        }
        return super.onOptionsItemSelected(item)

    }

    /**
     * Function is used to show the list of inserted data.
     */
    private fun setupListofDataIntoRecyclerView() {

        if (getItemsList().size > 0) {

            rvItemsList.visibility = View.VISIBLE
            tvNoRecordsAvailable.visibility = View.GONE

            // Set the LayoutManager that this RecyclerView will use.
            rvItemsList.layoutManager = LinearLayoutManager(this)
            // Adapter class is initialized and list is passed in the param.
            val itemAdapter = ItemPartAdapter(this, getItemsList())
            // adapter instance is set to the recyclerview to inflate the items.
            rvItemsList.adapter = itemAdapter
        } else {

            rvItemsList.visibility = View.GONE
            tvNoRecordsAvailable.visibility = View.VISIBLE
        }
    }
    /**
     * Function is used to get the Items List from the database table.
     */
    private fun getItemsList(): ArrayList<PartModelClass> {
        //creating the instance of DatabaseHandler class
        val databaseHandler: DatabaseHandler = DatabaseHandler(this)
        //calling the viewEmployee method of DatabaseHandler class to read the records
        val partList: ArrayList<PartModelClass> = databaseHandler.viewPart()


        return partList
    }

    //Method for saving the employee records in database
    private fun addRecord() {
        val no = etPartNo.text.toString()
        val name = etPartName.text.toString()
        val fgpdcode = etFgcode.text.toString()
        val linename = etLineName.text.toString()
        val databaseHandler: DatabaseHandler = DatabaseHandler(this)
        if (!name.isEmpty() && !no.isEmpty() && !fgpdcode.isEmpty() && !linename.isEmpty()) {
            val status =
                    databaseHandler.addPart(PartModelClass(0, no, name,fgpdcode,linename))
            if (status > -1) {
                Toast.makeText(applicationContext, "Record saved", Toast.LENGTH_LONG).show()
                etPartNo.text.clear()
                etPartName.text.clear()
                etFgcode.text.clear()
                etLineName.text.clear()

                setupListofDataIntoRecyclerView()
                //bindDataSpinner()
            }
        } else {
            Toast.makeText(
                    applicationContext,
                    "Name or Email cannot be blank",
                    Toast.LENGTH_LONG
            ).show()
        }
    }

    /**
     * Method is used to show the Custom Dialog.
     */
    fun updateRecordDialog(partModelClass: PartModelClass) {
        val updateDialog = Dialog(this, R.style.Theme_Dialog)
        updateDialog.setCancelable(false)
        /*Set the screen content from a layout resource.
         The resource will be inflated, adding all top-level views to the screen.*/
        updateDialog.setContentView(R.layout.dialog_update)

        updateDialog.etUpdatePartNo.setText(partModelClass.partNo)
        updateDialog.etUpdatePartName.setText(partModelClass.partName)
        updateDialog.etUpdateFgCode.setText(partModelClass.fgpdCode)
        updateDialog.etUpdateLineName.setText(partModelClass.lineName)

        updateDialog.tvUpdate.setOnClickListener(View.OnClickListener {

            val no = updateDialog.etUpdatePartNo.text.toString()
            val name = updateDialog.etUpdatePartName.text.toString()
            val fgpdcode = updateDialog.etUpdateFgCode.text.toString()
            val line = updateDialog.etUpdateLineName.text.toString()


            val databaseHandler: DatabaseHandler = DatabaseHandler(this)

            if (!name.isEmpty() && !no.isEmpty() && !fgpdcode.isEmpty() && !line.isEmpty() ) {
                val status =
                        databaseHandler.updatePart(PartModelClass(partModelClass.id, no, name,fgpdcode,line))
                if (status > -1) {
                    Toast.makeText(applicationContext, "Record Updated.", Toast.LENGTH_LONG).show()

                    setupListofDataIntoRecyclerView()

                    updateDialog.dismiss() // Dialog will be dismissed
                    //bindDataSpinner()
                }
            } else {
                Toast.makeText(
                        applicationContext,
                        "Name or Email cannot be blank",
                        Toast.LENGTH_LONG
                ).show()
            }
        })
        updateDialog.tvCancel.setOnClickListener(View.OnClickListener {
            updateDialog.dismiss()
        })
        //Start the dialog and display it on screen.
        updateDialog.show()
    }

    /**
     * Method is used to show the Alert Dialog.
     */
    fun deleteRecordAlertDialog(partModelClass: PartModelClass) {
        val builder = AlertDialog.Builder(this)
        //set title for alert dialog
        builder.setTitle("Delete Record")
        //set message for alert dialog
        builder.setMessage("Are you sure you wants to delete ${partModelClass.partName}.")
        builder.setIcon(android.R.drawable.ic_dialog_alert)

        //performing positive action
        builder.setPositiveButton("Yes") { dialogInterface, which ->

            //creating the instance of DatabaseHandler class
            val databaseHandler: DatabaseHandler = DatabaseHandler(this)
            //calling the deleteEmployee method of DatabaseHandler class to delete record
            val status = databaseHandler.deletePart(PartModelClass(partModelClass.id, "", "", fgpdCode = "", lineName = ""))
            if (status > -1) {
                Toast.makeText(
                        applicationContext,
                        "Record deleted successfully.",
                        Toast.LENGTH_LONG
                ).show()

                setupListofDataIntoRecyclerView()
            }

            dialogInterface.dismiss() // Dialog will be dismissed
        }
        //performing negative action
        builder.setNegativeButton("No") { dialogInterface, which ->
            dialogInterface.dismiss() // Dialog will be dismissed
        }
        // Create the AlertDialog
        val alertDialog: AlertDialog = builder.create()
        // Set other dialog properties
        alertDialog.setCancelable(false) // Will not allow user to cancel after clicking on remaining screen area.
        alertDialog.show()  // show the dialog to UI
    }

    override fun onBackPressed() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        super.onBackPressed()
    }
}