package com.mazenrashed.universalbluetoothprinter

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteException
import android.database.sqlite.SQLiteOpenHelper

class DatabaseHandler (context: Context) :
        SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

    companion object {
        private val DATABASE_VERSION = 1
        private val DATABASE_NAME = "PartDatabase"

        private val TABLE_CONTACTS = "PartTable"

        private val KEY_ID = "_id"
        private val KEY_PARTNO = "partno"
        private val KEY_PARTNAME = "partname"
        private val KEY_FGPDCODE = "fgpdcode"
        private val KEY_LINENAME = "linename"

    }

    override fun onCreate(db: SQLiteDatabase?) {
        //creating table with fields
        val CREATE_CONTACTS_TABLE = ("CREATE TABLE " + TABLE_CONTACTS + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_PARTNO + " TEXT,"
                + KEY_PARTNAME + " TEXT," + KEY_FGPDCODE + " TEXT,"
                + KEY_LINENAME + " TEXT" + ")")
        db?.execSQL(CREATE_CONTACTS_TABLE)
    }
    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db!!.execSQL("DROP TABLE IF EXISTS $TABLE_CONTACTS")
        onCreate(db)
    }
    /**
     * Function to insert data
     */
    fun addPart(part: PartModelClass): Long {
        val db = this.writableDatabase

        val contentValues = ContentValues()
        contentValues.put(KEY_PARTNO, part.partNo)
        contentValues.put(KEY_PARTNAME, part.partName)
        contentValues.put(KEY_FGPDCODE, part.fgpdCode)
        contentValues.put(KEY_LINENAME, part.lineName)

        // Inserting employee details using insert query.
        val success = db.insert(TABLE_CONTACTS, null, contentValues)
        //2nd argument is String containing nullColumnHack

        db.close() // Closing database connection
        return success
    }

    //Method to read the records from database in form of ArrayList
    fun viewPart(): ArrayList<PartModelClass> {

        val empList: ArrayList<PartModelClass> = ArrayList<PartModelClass>()

        // Query to select all the records from the table.
        val selectQuery = "SELECT  * FROM $TABLE_CONTACTS"

        val db = this.readableDatabase
        // Cursor is used to read the record one by one. Add them to data model class.
        var cursor: Cursor? = null

        try {
            cursor = db.rawQuery(selectQuery, null)

        } catch (e: SQLiteException) {
            db.execSQL(selectQuery)
            return ArrayList()
        }

        var id: Int
        var partno: String
        var partname: String
        var fgpdcode: String
        var linename: String

        if (cursor.moveToFirst()) {
            do {
                id = cursor.getInt(cursor.getColumnIndex(KEY_ID))
                partno = cursor.getString(cursor.getColumnIndex(KEY_PARTNO))
                partname = cursor.getString(cursor.getColumnIndex(KEY_PARTNAME))
                fgpdcode = cursor.getString(cursor.getColumnIndex(KEY_FGPDCODE))
                linename = cursor.getString(cursor.getColumnIndex(KEY_LINENAME))

                val emp = PartModelClass(id = id, partNo = partno, partName = partname,fgpdCode = fgpdcode,lineName = linename)
                empList.add(emp)

            } while (cursor.moveToNext())
        }
        return empList
    }

    fun viewPartByName(partName:String): PartModelClass? {
        var part: PartModelClass = PartModelClass(0,"","","","")

        // Query to select all the records from the table.
        val selectQuery = "SELECT  * FROM $TABLE_CONTACTS WHERE $KEY_PARTNAME = '${partName}'"

        val db = this.readableDatabase
        // Cursor is used to read the record one by one. Add them to data model class.
        var cursor: Cursor? = null

        try {
            cursor = db.rawQuery(selectQuery, null)

        } catch (e: SQLiteException) {
            db.execSQL(selectQuery)
            return null
        }

        var id: Int
        var partno: String
        var partname: String
        var fgpdcode: String
        var linename: String

        if (cursor.moveToFirst()) {
            do {
                id = cursor.getInt(cursor.getColumnIndex(KEY_ID))
                partno = cursor.getString(cursor.getColumnIndex(KEY_PARTNO))
                partname = cursor.getString(cursor.getColumnIndex(KEY_PARTNAME))
                fgpdcode = cursor.getString(cursor.getColumnIndex(KEY_FGPDCODE))
                linename = cursor.getString(cursor.getColumnIndex(KEY_LINENAME))

                val p = PartModelClass(id = id, partNo = partno, partName = partname,fgpdCode = fgpdcode,lineName = linename)
                part = p

            } while (cursor.moveToNext())
        }
        return part
    }

    /**
     * Function to update record
     */
    fun updatePart(part: PartModelClass): Int {
        val db = this.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(KEY_PARTNO, part.partNo)
        contentValues.put(KEY_PARTNAME, part.partName)
        contentValues.put(KEY_FGPDCODE, part.fgpdCode)
        contentValues.put(KEY_LINENAME, part.lineName)

        // Updating Row
        val success = db.update(TABLE_CONTACTS, contentValues, KEY_ID + "=" + part.id, null)
        //2nd argument is String containing nullColumnHack

        // Closing database connection
        db.close()
        return success
    }

    /**
     * Function to delete record
     */
    fun deletePart(part: PartModelClass): Int {
        val db = this.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(KEY_ID, part.id) // EmpModelClass id
        // Deleting Row
        val success = db.delete(TABLE_CONTACTS, KEY_ID + "=" + part.id, null)
        //2nd argument is String containing nullColumnHack

        // Closing database connection
        db.close()
        return success
    }

}